<?php
include_once 'init.php';


// ------------------
// Déclaration des variables par défaut
// --
// On utilisera ces variables dans le formulaire (attribut "value" des champs)
// Les valeurs NULL seront surchargées avec les valeur saisies par l'utilisateur
// ------------------

$title          = null;
$actors         = null;
$directors      = null;
$producer       = null;
$year_of_prod   = null;
$language       = null;
$category       = null;
$storyline      = null;
$video          = null;



// ------------------
// PARTIE 1
// ------------------
// ON GERE L'ENVOIS DU FORMULAIRE
// Le tableau de la "super globale" $_POST sera généré UNIQUEMENT lors de l'envois du
// formulaire avec la méthode POST (<form method="post">)
// ------------------
if (!empty($_POST)) {

  $send = true;
  // Recupération des données du formulaire
  // --

  $title          = isset($_POST['title'])           ?      $_POST['title']        : null;
  $actors         = isset($_POST['actors'])          ?      $_POST['actors']       : null;
  $directors      = isset($_POST['directors'])       ?      $_POST['directors']    : null;
  $producer       = isset($_POST['producer'])        ?      $_POST['producer']     : null;
  $year_of_prod   = isset($_POST['year_of_prod'])    ?      $_POST['year_of_prod'] : null;
  $language       = isset($_POST['language'])        ?      $_POST['language']     : null;
  $category       = isset($_POST['category'])        ?      $_POST['category']     : null;
  $storyline      = isset($_POST['storyline'])       ?      $_POST['storyline']    : null;
  $video          = isset($_POST['video'])           ?      $_POST['video']        : null;



  // Contrôle qu'il y ait au minimum 5 caractères:
  // Pour le titre, le nom du réalisateur, les acteurs, le producteur et la synopsis

  if (strlen($title) < 5) {
    $send = false;
    setFlashbag("danger", "Vérifier que le titre comporte au minimum 5 caractères!");
  }

  if (strlen($directors) < 5) {
    $send = false;
    setFlashbag("danger", "Vérifier que le nom du réalisateur comporte au minimum 5 caractères!");
  }

  if (strlen($actors) < 5) {
    $send = false;
    setFlashbag("danger", "Vérifier que les acteurs comporte au minimum 5 caractères!");
  }

  if (strlen($producer) < 5) {
    $send = false;
    setFlashbag("danger", "Vérifier que le producteur comporte au minimum 5 caractères!");
  }

  if (strlen($storyline) < 5) {
    $send = false;
    setFlashbag("danger", "Vérifier que la synopsis comporte au minimum 5 caractères!");
  }

  // Contrôle de l'URL pour la vidéo
  if(filter_var($video, FILTER_VALIDATE_URL)) {
    //Validation d'un lien.
  } else {
    $send = false;
    setFlashbag("danger", "Vérifier que l'URL soit valide pour la bande annonce!'");
}


  if ($send) {
    // Requête d'insertion d'un auteur dans la BDD
    $query_string = "INSERT INTO `movies` (`title`, `actors`, `directors`, `producer`, `year_of_prod`, `language`, `category`, `storyline`, `video`)
              VALUES (:title, :actors, :directors, :producer, :year_of_prod, :language, :category, :storyline, :video)";

    // On demande à PDO de préparer la requête
    $pdo = $pdo->prepare($query_string);

    // On prépare les "variables" de PDO

    $pdo->bindValue(":title"            , $title,            PDO::PARAM_STR);
    $pdo->bindValue(":actors"           , $actors,           PDO::PARAM_STR);
    $pdo->bindValue(":directors"        , $directors,        PDO::PARAM_STR);
    $pdo->bindValue(":producer"         , $producer,         PDO::PARAM_STR);
    $pdo->bindValue(":year_of_prod"     , $year_of_prod,     PDO::PARAM_STR);
    $pdo->bindValue(":language"         , $language,         PDO::PARAM_STR);
    $pdo->bindValue(":category"         , $category,         PDO::PARAM_STR);
    $pdo->bindValue(":storyline"        , $storyline,        PDO::PARAM_STR);
    $pdo->bindValue(":video"            , $video,            PDO::PARAM_STR);

    // On exécute la requête
    $pdo->execute();

    // Message de réussite lors d'un ajout
    setFlashbag("sucess", "Votre film a été ajouté à la base de données avec succès!");
  }

  header("location: films.php");
  exit;
}

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Ajouter votre film</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
    integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  </head>

  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="exo3.php">EXERCICE 3</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">

            <!-- Navigation utilisateur -->
            <ul class="nav navbar-nav navbar-right">

              <li><a href="auteurs.php">Page des films</a></li>

            </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
<br><br><br>

    <?php getFlashbag(); ?>

    <form method="post">

      <!-- // ● Les champs : année de production, langue, category, seront obligatoirement un
      // menu déroulant

    title, actors, directors, producer, year_of_prod, language, category, storyline, video
  -->
      <div class="container">

        <div class="form-group">
          <label for="title">TITRE</label><br>
          <input type="text" id="title" name="title" value="<?php echo $title; ?>">
        </div>

        <div class="form-group">
          <label for="actors">ACTEURS</label><br>
          <input type="text" id="actors" name="actors" value="<?php echo $actors; ?>">
        </div>

        <div class="form-group">
          <label for="directors">DIRECTEURS</label><br>
          <input type="text" id="directors" name="directors" value="<?php echo $directors; ?>">
        </div>

        <div class="form-group">
          <label for="producer">PRODUCTEUR</label><br>
          <input type="text" id="producer" name="producer" value="<?php echo $producer; ?>">
        </div>

        <div class="form-group">
          <label for="year_of_prod">ANNEE DE PRODUCTION</label><br>
          <select id="year_of_prod" name="year_of_prod">
            <option value="">--Choisir--</option>
            <?php for($i=date('Y'); $i>date('Y')-100; $i--): ?>
              <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
            <?php endfor; ?>
          </select>
        </div>

        <div class="form-group">
          <label for="language">LANGUE</label><br>
          <select id="language" name="language">
            <option value="<?php echo $language; ?>"> Français</option>
            <option value="<?php echo $language; ?>"> English</option>
            <option value="<?php echo $language; ?>"> Español</option>
            <option value="<?php echo $language; ?>"> Deutsch</option>
          </select>
        </div>

        <div class="form-group">
          <label for="category">CATEGORIE</label><br>
          <select id="category" name="category">
            <option value="<?php echo $category; ?>"> Aventure</option>
            <option value="<?php echo $category; ?>"> Policier</option>
            <option value="<?php echo $category; ?>"> Action</option>
            <option value="<?php echo $category; ?>"> Horreur</option>
          </select>
        </div>

        <div class="form-group">
          <label for="storyline">SYNOPSIS</label><br>
          <textarea type="text" id="storyline" name="storyline" value="<?php echo $storyline; ?>">Entrer la Synopsis</textarea>
        </div>

        <div class="form-group">
          <label for="video">VIDEO</label><br>
          <input type="text" id="video" name="video" value="<?php echo $video; ?>">
        </div>

        <button class="btn btn-primary" type="submit">Ajouter le film</button>
      </div>
    </form>


  </body>
</html>
