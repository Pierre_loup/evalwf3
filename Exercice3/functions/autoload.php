<?php

$fncDirectory = scandir('./functions/');

foreach ($fncDirectory as $file) {
    if ( preg_match("/^fnc-.*\.php$/i", $file) ) {
        include_once './functions/'.$file;
    }
}
