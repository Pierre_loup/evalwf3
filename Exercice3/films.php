<?php
include_once 'init.php';

// Definition de la chaine de la requête
$query_string = "SELECT title, actors, directors, producer, year_of_prod, language, category, storyline, video FROM movies";

// Definition de la requête pour PDO
$pdo = $pdo->query($query_string);


// PDO::FETCH_ASSOC - Retourne le tableau du résultat de la requete
$films = $pdo->fetchAll( PDO::FETCH_ASSOC );


?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Page des films</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
    integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="exo3.php">EXERCICE 3</a>
        </div>

      </div>
    </nav>
    <br><br><br>

    <div class="container">

      <?php getFlashbag(); ?>
      <table class="table table-hover">

        <tr class="info">
          <th>TITRE</th>
          <th>ACTEURS</th>
          <th>DIRECTEURS</th>
          <th>PRODUCTEUR</th>
          <th>ANNEE DE PRODUCTION</th>
          <th>LANGUE</th>
          <th>CATEGORIE</th>
          <th>SYNOPSIS</th>
          <th>VIDEO</th>

        </tr>

        <!-- Boucle du tableau  -->
        <?php foreach($films as $film): ?>
          <tr>
            <td><?php echo $film['title']; ?></td>
            <td><?php echo $film['actors']; ?></td>
            <td><?php echo $film['directors']; ?></td>
            <td><?php echo $film['producer']; ?></td>
            <td><?php echo $film['year_of_prod']; ?></td>
            <td><?php echo $film['language']; ?></td>
            <td><?php echo $film['category']; ?></td>
            <td><?php echo $film['storyline']; ?></td>
            <td><?php echo $film['video']; ?></td>
          </tr>

        <?php endforeach; ?>
      </table>

      <?php
      echo "<br>";
      echo "<a href=\"filmadd.php\">Ajouter un film</a>";

      ?>

    </div>


  </body>
</html>
