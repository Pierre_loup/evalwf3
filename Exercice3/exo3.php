<?php
# Exercice 3

# Étape 1 :
// Cette table, nommée “movies” sera composée des champs suivants :
// ● title (varchar) : le nom du film
// ● actors (varchar) : les noms d’acteurs
// ● director (varchar) : le nom du réalisateur
// ● producer (varchar) : le nom du producteur
// ● year_of_prod (year) : l’année de production
// ● language (varchar) : la langue du film
// ● category (enum) : la catégorie du film
// ● storyline (text) : le synopsis du film
// ● video (varchar) : le lien de la bande annonce du film
// N’oubliez pas de créer un ID pour chaque film et de l’auto-incrémenter.

# Étape 2 :
// Créer un formulaire permettant d’ajouter un film et effectuer les vérifications nécessaires.
// Prérequis :
// ● Les champs “titre, nom du réalisateur, acteurs, producteur et synopsis” comporteront
// au minimum 5 caractères.
// ● Les champs : année de production, langue, category, seront obligatoirement un
// menu déroulant
// ● Le lien de la bande annonce sera obligatoirement une URL valide
// ● En cas d’erreurs de saisie, des messages d’erreurs seront affichés en rouge
// Chaque film sera ajouté à la base de données créée. Un message de réussite confirmera
// l’ajout du film.

# Étape 3 :
// Créer une page listant dans un tableau HTML les films présents dans la base de données.
// Ce tableau ne contiendra, par film, que le nom du film, le réalisateur et l’année de
// production.
// Une colonne de ce tableau contiendra un lien « plus d’infos » permettant de voir la fiche
// d’un film dans le détail.

# Étape 4 :
// Créer une page affichant le détail d’un film de manière dynamique. Si le film n’existe pas,
// une erreur sera affichée.

include_once 'init.php';

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Exercice 3 - Films</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
    integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="exo3.php">EXERCICE 3</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">

            <!-- Navigation utilisateur -->
            <ul class="nav navbar-nav navbar-right">

              <li><a href="films.php">Page des Films</a></li>

            </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
  </body>
</html>



























 ?>
