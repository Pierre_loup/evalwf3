-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Mer 12 Juillet 2017 à 17:19
-- Version du serveur :  10.1.21-MariaDB
-- Version de PHP :  5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `exercice_3`
--

-- --------------------------------------------------------

--
-- Structure de la table `movies`
--

CREATE TABLE `movies` (
  `id` int(11) NOT NULL,
  `title` varchar(40) NOT NULL,
  `actors` varchar(40) NOT NULL,
  `directors` varchar(40) NOT NULL,
  `producer` varchar(40) NOT NULL,
  `year_of_prod` year(4) NOT NULL,
  `language` varchar(40) NOT NULL,
  `category` enum('') NOT NULL,
  `storyline` text NOT NULL,
  `video` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `movies`
--

INSERT INTO `movies` (`id`, `title`, `actors`, `directors`, `producer`, `year_of_prod`, `language`, `category`, `storyline`, `video`) VALUES
(1, 'firstmovie', 'john', 'nolan', 'steven', 0000, '', '', 'testetetstettste', 'aucune'),
(2, 'Second', 'Jake', 'Steven', 'Spielberg', 0000, '', '', 'aucune', 'pareil'),
(3, 'third', 'Martin', 'George', 'personne', 1984, '', '', 'test', 'zero'),
(4, 'aze', 'aze', 'aze', 'aze', 2012, '', '', 'Entrer la Synopsis', 'az'),
(5, 'aze', 'aze', 'aze', 'aze', 2012, '', '', 'aze', 'aze'),
(6, 'azrraer', 'zegaftrf', 'zegaerdf', 'dfqgrgqdf', 2000, '', '', 'Entrer la Synopsis', 'qgsdfg'),
(7, 'nouveau', 'nouveau', 'nouveau', 'nouveau', 1923, '', '', 'Entrer la Synopsis', 'http://getbootstrap.com/getting-started/');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `movies`
--
ALTER TABLE `movies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
