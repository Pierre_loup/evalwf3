<?php
# Exercice 2
// Créer une fonction permettant de convertir un montant en euros vers un montant en dollars
// américains.
// Cette fonction prendra deux paramètres :
// ● Le montant de type int ou float
// ● La devise de sortie (uniquement EUR ou USD).
// Si le second paramètre est “USD”, le résultat de la fonction sera, par exemple :
// 1 euro = 1.085965 dollars américains
// Il faut effectuer les vérifications nécessaires afin de valider les paramètres

$euro = 0;

if (isset($_POST['validate'])) {

    $euro = isset($_POST['valeur']) ? trim($_POST['valeur']) : null;

    // Indique à l'utilisateur qu'il doit rentrer une valeur de type INT ou Float
    if (!is_numeric($euro)) {
      echo "Veuillez saisir une valeur numérique!";
    }

    // Fonction converstissant une valeur Euro en Dollars
    function convert($euros,$exchange)
    {
      $euros = $euros*$exchange;
      return $euros;
    }

$convert = convert($_POST['valeur'],1.085965);
}
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Convertisseur €/$ - Exercice 2</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
    integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
    crossorigin="anonymous">
  </head>

  <body>

    <div class="container">
      <h1>EXERCICE 2</h1><hr>

      <!-- Formulaire -->
      <form class="form-group" method="POST" name="convert">
        <label for="valeur">Entrer une valeur en Euro (€):</label><br>
        <input type="text" name="valeur" id="valeur">
        <input class="btn btn-primary"type="submit" name="validate" value="Convertir">
      </form>

      <hr>

      <!-- Résultat en Euro -->
      <div>
        <label>Euro(€) en Dollar ($):</label><br>
         <?php
         global $convert;
         if (isset($convert)){
         echo $euro."€ = $".$convert;
         }
         ?>
      </div>


    </div>

  </body>
</html>
