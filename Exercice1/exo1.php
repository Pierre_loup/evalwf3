<?php
# Exercice 1
// Créer un tableau en PHP contenant les infos suivantes :
// ● Prénom
// ● Nom
// ● Adresse
// ● Code Postal
// ● Ville
// ● Email
// ● Téléphone
// ● Date de naissance au format anglais (YYYY-MM-DD)
// A l’aide d’une boucle, afficher le contenu de ce tableau (clés + valeurs) dans une liste HTML.
// La date sera affichée au format français (DD/MM/YYYY).
// Bonus :
// Gérer l’affichage de la date de naissance à l’aide de la classe DateTime

// Tableau des données
$array = [
  'Prénom'            => 'Pierre-loup',
  'Nom'               => 'Peeren',
  'Adresse'           => '32 avenue de la République',
  'Code Postal'       => '59110',
  'Ville'             => 'La Madeleine',
  'Email'             => 'peeren.pierreloup@gmail.com',
  'Téléphone'         => '0695598228',
  'Naissance'         => '2000-07-12',
];

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Tableau PHP - Exercice 1</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
    integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
    crossorigin="anonymous">
  </head>

  <body>

    <div class="container">
    <h1>EXERCICE 1</h1><hr>
    <table class="table table-hover">

      <!-- Boucle du tableau  -->
      <?php foreach($array as $key => $value): ?>

        <ul>
          <li><?php if( $key == 'Naissance') {
              $date = new DateTime($value);
              echo $key." : ". $date->format('d/m/Y');
              } else {
              echo $key." : ". $value;
              } ?>
          </li>
        </ul>

      <?php endforeach; ?>
    </table>

    </div>
  </body>
</html>
